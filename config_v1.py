def get_data_vals():
	import numpy as np
	import pandas as pd
	path = 'D:\\Oak-Network\\03-20-2023 (Trial Site Code Update)\\Main\\'
	energy_daily = pd.read_csv(path + "energy_daily.csv", parse_dates=["Time stamp"])
	energy_hourly = pd.read_csv(path + "energy_hourly.csv", parse_dates=["Time stamp"])
	power_df = pd.read_csv(path + "power_df_hourly.csv", parse_dates=["Time stamp"])
	current_df = pd.read_csv(path + "current_df_daily.csv", parse_dates=["Time stamp"])
	power_factor = pd.read_csv(path + "power_factor_hourly.csv", parse_dates=["Time stamp"])
	voltage_df = pd.read_csv(path + "voltage_hourly.csv", parse_dates=["Time stamp"])
	work_hr = pd.read_excel(path + "working_hours.xlsx")
	energy_col = 'Rudies Kitchen  - Total  (kWh)'
	current_col = 'Main 1 L1   (A)'
	power_col = 'Rudies Kitchen  - Total  (kW)'
	pf_col = 'Main 1   ()'
	voltage_col = 'Main 1   (V)'
	tariff = 0.1758
	day = 0.2847
	night = 0.1715
	reactive_tariff = 0.005
	equip_energy = pd.read_csv(path+"ac_energy_hourly.csv", parse_dates = ["Time stamp"])    
	equipment = ['AC 1   (kWh)']
	equipment_type = "ACs"
	equipment_total = "AC  - Total  (kWh)"
	temp_col = 'Rudies Kitchen temperatures (℃)'
	equipment_phase_df = pd.read_csv(path+"ac_phase_current.csv", parse_dates = ["Time stamp"])
	equip_phase = ['AC 1 L1   (A)', 'AC 1 L2   (A)', 'AC 1 L3   (A)']
	return energy_daily, energy_hourly, work_hr, energy_col, current_df, current_col, voltage_df, voltage_col, power_df, power_col, power_factor, pf_col, path, tariff, reactive_tariff, equip_energy, equipment, equipment_type, equipment_total, temp_col, equipment_phase_df, equip_phase